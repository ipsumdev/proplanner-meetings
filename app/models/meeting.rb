class Meeting < ApplicationRecord
  #TO DO: implement associations
  #has_many :invitees
  #belongs_to :project
  validates :title, :start_date, :end_date, :meeting_type, :invitee, :attendance, :primary_resource, :primary_resource_type, :secondary_resource, :secondary_resource_type, presence: true 
end
