Rails.application.routes.draw do
  apipie
  resources :meetings
  resources :user_profiles
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
