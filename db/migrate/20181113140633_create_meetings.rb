class CreateMeetings < ActiveRecord::Migration[5.2]
  def change
    create_table :meetings do |t|
      t.string :title, null: false
      t.string :description
      t.datetime :start_date, null: false
      t.datetime :end_date, null: false
      t.integer :meeting_type, default: 0, null: false
      t.string :attendance, array: true, default: []
      t.string :invitee, array: true, default: []
      t.integer :primary_resource, null: false
      t.integer :primary_resource_type, null: false
      t.integer :secondary_resource, null: false
      t.integer :secondary_resource_type, null: false

      t.timestamps
    end
  end
end
