# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User.create(email: "alvaro@ipsumapp.co")
# User.create(email: "pia@ipsumapp.co")
# User.create(email: "sebastian@ipsumapp.co")
# User.create(email: "bernardo@ipsumapp.co")
# User.create(email: "isaias@ipsumapp.co")
# User.create(email: "karen@ipsumapp.co")
# User.create(email: "jonathan@ipsumapp.co")
# User.create(email: "felipe@ipsumapp.co")

# # [UserProfile] name:string image_url:string company_id:integer role:string
# UserProfile.create(name: "Alvaro Urbina", image_url: "", company_id: 1, role: "admin", user: User.where(email: "alvaro@ipsumapp.co").first)
# UserProfile.create(name: "Pia Gomez", image_url: "", company_id: 1, role: "admin", user: User.where(email: "pia@ipsumapp.co").first)
# UserProfile.create(name: "Sebastian Machuca", image_url: "", company_id: 1, role: "admin", user: User.where(email: "sebastian@ipsumapp.co").first)
# UserProfile.create(name: "Bernardo Machuca", image_url: "", company_id: 1, role: "admin", user: User.where(email: "bernardo@ipsumapp.co").first)
# UserProfile.create(name: "Isaias Lopez", image_url: "", company_id: 1, role: "admin", user: User.where(email: "isaias@ipsumapp.co").first)
# UserProfile.create(name: "Karen Guerra", image_url: "", company_id: 1, role: "admin", user: User.where(email: "karen@ipsumapp.co").first)
# UserProfile.create(name: "Jonathan Ramirez", image_url: "", company_id: 1, role: "admin", user: User.where(email: "jonathan@ipsumapp.co").first)
# UserProfile.create(name: "Felipe Cabargas", image_url: "", company_id: 1, role: "admin", user: User.where(email: "felipe@ipsumapp.co").first)

# meeting = Meeting.create([
#           { name: "Example Meeting 1", description: "This is a demo meeting description.", start_date: DateTime.now, end_date: DateTime.now+3.hours, meeting_type: 1, user: User.first, user_profile_ids: [1,3,5,7], attendance: [3,5,7]},
#           { name: "Example Meeting 2", description: "This is a demo meeting description.", start_date: DateTime.now, end_date: DateTime.now+6.hours, meeting_type: 1, user: User.second, user_profile_ids: [2,4,6,8], attendance: [2]},
#           { name: "Example Meeting 3", description: "This is a demo meeting description.", start_date: DateTime.now, end_date: DateTime.now+2.days, meeting_type: 1, user: User.last, user_profile_ids: [1,2,3,4], attendance: [1,2,3,4]}
#         ])