require 'rails_helper'

RSpec.describe MeetingsController, :type => :controller do
  describe 'GET #index' do
    before do
      meeting = create(:meeting)
      get :index
    end

    it 'returns http success' do
      expect(response).to be_successful
    end

    it 'show a list of all meetings' do
      expect(response.body).to eq(Meeting.all.to_json)
    end
  end

  describe 'GET #show' do
    before do
      meeting = create(:meeting)
      get :show, params: { id: meeting.id }
    end

    it 'returns http success' do
      expect(response).to be_successful
    end

    it 'assigns the @meeting' do
      expect(assigns(:meeting)).not_to be_nil
    end

    it 'assigns the requested meeting to @meeting' do
      expect(assigns(:meeting)).to match(Meeting.last)
    end

    it 'returns json of @meeting' do
      expect(response.body).to eq(
        {
          meeting: assigns(:meeting)
        }.to_json
      )
    end
  end

  describe 'POST #create' do
    context 'with valid parameters' do
      let(:valid_params) do
        { meeting: {
           title: "Testing post create method in a meeting",
           description: "Esta es una reunión de prueba con rspec",
           start_date: DateTime.now,
           end_date: DateTime.now+2.hours,
           meeting_type: 0,
           invitee: ["1", "2", "3"],
           attendance: ["1", "2"],
           primary_resource: 1,
           primary_resource_type: 2,
           secondary_resource: 2,
           secondary_resource_type: 3
          }
        }
      end

      it 'creates a new meeting' do
        expect { post :create, params: valid_params }.to change(Meeting, :count).by(+1)
        expect(response).to have_http_status :created
      end

      context 'with invalid parameters' do
        let(:invalid_params) do
          { meeting: {
             title: nil,
             description: nil,
             start_date: nil,
             end_date: nil,
             meeting_type: nil,
             invitee: ["1", "2", "3"],
             attendance: ["1", "2"],
             primary_resource: 1,
             primary_resource_type: 2,
             secondary_resource: 2,
             secondary_resource_type: 3
            }
          }
        end

        it 'does not create a new meeting' do
          expect { post :create, params: invalid_params }.to change(Meeting, :count).by(0)
          expect(response).to have_http_status :unprocessable_entity
        end
      end
    end
  end

  describe 'PATCH #update' do
    let!(:meeting) { create :meeting }

    it 'updates meeting info' do
      params = {
      title:'PRUEBA'
      }

      patch :update, params: { id: meeting.id, meeting: params }
      meeting.reload
      params.keys.each do |key|
        expect(meeting.attributes[key.to_s]).to eq params[key]
      end
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/meetings/1').to route_to('meetings#update', id: '1')
    end
  end

  describe 'DELETE #destroy' do
    let!(:meeting) { create :meeting }

    it 'deletes the meeting' do
      expect { delete :destroy, params: { id: meeting.id } }.to change(Meeting, :count).by(-1)
    end
  end
end
