FactoryBot.define do
  factory :meeting do
    title { "Reunión de prueba" }
    description { "Esta es una reunión de prueba con rspec" }
    start_date { DateTime.now }
    end_date { DateTime.now+2.hours }
    meeting_type { 0 }
    invitee { ["1", "2", "3", "4"] }
    attendance { ["1", "2", "3"] }
    primary_resource { 1 }
    primary_resource_type { 2 }
    secondary_resource { 2 }
    secondary_resource_type { 3 }
  end
end
