require 'rails_helper'

RSpec.describe  Meeting, :type => :model do
  it 'is valid with valid attributes' do
    meeting = create(:meeting)
    expect(meeting).to be_valid
  end

  it 'is not valid without a title' do
    meeting = Meeting.new(title: nil,
                          start_date: DateTime.now,
                          end_date: DateTime.now + 3.hours,
                          meeting_type: 0,
                          invitee: ["1", "2", "3", "4"],
                          attendance: ["1", "2", "3"],
                          primary_resource: 1,
                          primary_resource_type: 2,
                          secondary_resource: 2,
                          secondary_resource_type: 3)
    expect(meeting).to_not be_valid
  end

  it 'is not valid without a start_date' do
    meeting = Meeting.new(title: "Meeting 1",
                          start_date: nil,
                          end_date: DateTime.now + 3.hours,
                          meeting_type: 0,
                          invitee: ["1", "2", "3", "4"],
                          attendance: ["1", "2", "3"],
                          primary_resource: 1,
                          primary_resource_type: 2,
                          secondary_resource: 2,
                          secondary_resource_type: 3)
    expect(meeting).to_not be_valid
  end

  it 'is not valid without a end_date' do
    meeting = Meeting.new(title: "Meeting 1",
                          start_date: DateTime.now,
                          end_date: nil,
                          meeting_type: 0,
                          invitee: ["1", "2", "3", "4"],
                          attendance: ["1", "2", "3"],
                          primary_resource: 1,
                          primary_resource_type: 2,
                          secondary_resource: 2,
                          secondary_resource_type: 3)
    expect(meeting).to_not be_valid
  end

  # it 'defaults its meeting_type to 0 when not given' do
  #   meeting = Meeting.new(title: "Meeting 1",
  #                         start_date: DateTime.now,
  #                         end_date: DateTime.now+2.hours,
  #                         attendance: ["1", "2", "3"],
  #                         primary_resource: 1,
  #                         primary_resource_type: 2,
  #                         secondary_resource: 2,
  #                         secondary_resource_type: 3)
  #   expect(meeting.meeting_type).to_not be_valid
  # end

  it 'is not valid without a primary_resource' do
    meeting = Meeting.new(title: "Meeting 1",
                          start_date: DateTime.now,
                          end_date: DateTime.now + 4.hours,
                          meeting_type: 0,
                          invitee: ["1", "2", "3", "4"],
                          attendance: ["1", "2", "3"],
                          primary_resource: nil,
                          primary_resource_type: 2,
                          secondary_resource: 2,
                          secondary_resource_type: 3)
    expect(meeting).to_not be_valid
  end

  it 'is not valid without a primary_resource_type' do
    meeting = Meeting.new(title: "Meeting 1",
                          start_date: DateTime.now,
                          end_date: DateTime.now + 4.hours,
                          meeting_type: 0,
                          invitee: ["1", "2", "3", "4"],
                          attendance: ["1", "2", "3"],
                          primary_resource: 1,
                          primary_resource_type: nil,
                          secondary_resource: 2,
                          secondary_resource_type: 3)
    expect(meeting).to_not be_valid
  end

  it 'is not valid without a secondary_resource' do
    meeting = Meeting.new(title: "Meeting 1",
                          start_date: DateTime.now,
                          end_date: DateTime.now + 4.hours,
                          meeting_type: 0,
                          invitee: ["1", "2", "3", "4"],
                          attendance: ["1", "2", "3"],
                          primary_resource: 1,
                          primary_resource_type: 2,
                          secondary_resource: nil,
                          secondary_resource_type: 3)
    expect(meeting).to_not be_valid
  end

  it 'is not valid without a secondary_resource_type' do
    meeting = Meeting.new(title: "Meeting 1",
                          start_date: DateTime.now,
                          end_date: DateTime.now + 4.hours,
                          meeting_type: 0,
                          invitee: ["1", "2", "3", "4"],
                          attendance: ["1", "2", "3"],
                          primary_resource: 1,
                          primary_resource_type: 2,
                          secondary_resource: 2,
                          secondary_resource_type: nil)
    expect(meeting).to_not be_valid
  end
end
